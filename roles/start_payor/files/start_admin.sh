#! /bin/bash

DOMAIN_HOME="$BEA_HOME/user_projects/domains/$WL_DOMAIN"
TOOLS_BIN="$DOMAIN_HOME/tools/bin"

# Common functions. 
if [ -f $TOOLS_BIN/installCommon.bash ]; then
    . $TOOLS_BIN/installCommon.bash
else
    echo "installCommon.bash functions file not found."
    exit 1
fi

function startAdminServerConfirmRunning() {
    local LOG_DIR="${DOMAIN_HOME}/logs"
    local LOG_FILE="${LOG_DIR}/start_admin.log"
    mkdir -p $LOG_DIR

    # must run the command below in background; otherwise it will be "killed" by ansible, don't know why
    nohup $BEA_HOME/oracle_common/common/bin/wlst.sh $DOMAIN_HOME/tools/resource/start_admin.py $domainName $adminHost $adminPort $weblogicPassword > ${LOG_FILE} 2>&1 &

    ADMIN_STARTED="false"
    # wait up to 10 times, 2.5 minutes...
    local retry_wl_max=10
    local retry_wl=0
    while [[ "$ADMIN_STARTED" == "false" ]]; do
        if [[ ${retry_wl} -gt ${retry_wl_max} ]]; then
            exitWithErrorMessage "failed to start Admin Server. See detail in \"${LOG_FILE}\""
            break
        fi
        sleep 15
        local running_wl=$(grep "RUNNING" ${LOG_FILE})
        if [[ "X${running_wl}" == "X" ]]; then
            let retry_wl=retry_wl+1
            echo "Admin Server is not running yet, check ${retry_wl} ..."
        else
            echo "Admin Server is running."
            ADMIN_STARTED="true"
            break
        fi
    done
}

######
# Main
######

# Needed by installCommon.bash functions...
export domain_home="$DOMAIN_HOME"

setClasspath 1
validateEnvironmentVars

cd "$DOMAIN_HOME"

# Get parameters...
propfile=$domain_home/data/system_credentials.txt
adminHost=`get_prop $propfile ADMIN_HOST`
adminPort=`get_prop $propfile ADMIN_PORT`
weblogicPassword=`get_prop $propfile WEBLOGIC_PASSWORD`

domainName=$WL_DOMAIN

# Debug echo statements...
# echo "------------------------------------------------------------------"
# echo "CLASSPATH before calling start_nm_admin.py in start_nm_admin.sh :"
# echo $CLASSPATH
# echo "------------------------------------------------------------------"
PID=`ps -efw | grep -i "Xms" | grep -i "weblogic.Name=admin" | awk '{print $2}'`
if [[ ! -z "$PID"  ]] ; then
    echo "Admin server has already started on "`hostname`
else
    startAdminServerConfirmRunning
fi

