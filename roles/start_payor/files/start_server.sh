#!/bin/bash 

function startWebLogicConfirmRunning() {
    WEBLOGIC_STARTED="false"
    # wait up to 10 times, 5 minutes...
    local retry_wl_max=10
    local retry_wl=0
    local LOG_FILE="$domain_home/logs/startWebLogic.log"
    while [[ "$WEBLOGIC_STARTED" == "false" ]]; do
        if [[ ${retry_wl} -gt ${retry_wl_max} ]]; then
            # Weblogic Server failed to start
            break
        fi
        sleep 30
        local running_wl=$(grep "RUNNING" ${LOG_FILE})
        if [[ "X${running_wl}" == "X" ]]; then
            local failed_wl=$(grep "Server state changed to FAILED" ${LOG_FILE})
            if [[ "X${failed_wl}" == "X" ]]; then
                let retry_wl=retry_wl+1
                echo "Weblogic Server is not running yet, check ${retry_wl} ..."
            else
                # Weblogic Server failed to start
                break
            fi
        else
            echo "WebLogic Server is running."
            WEBLOGIC_STARTED="true"
            break
        fi
    done

    if [[ "${WEBLOGIC_STARTED}" == "false" ]]; then
        exitWithErrorMessage "failed to start WebLogic Server."
    fi
}

startSingleServer() {
    PID=`ps -efw | grep -i "Xms" | grep -i "weblogic.Name=claimserver" | awk '{print $2}'`
    if [[ ! -z "$PID"  ]] ; then
        echo "WebLogic Server has already started on "`hostname`
    else
        tools/bin/wlgo
        startWebLogicConfirmRunning
    fi
}

startMultiServers() {
    cd $toolsbin_dir
    ./start_nm_admin.sh
    cd $toolsbin_dir
    ./managed_servers.sh start
}

startAllServers() {

    if [[ "$installed_server_type" == "multi_server" ]]; then
        startMultiServers
    else
        startSingleServer
    fi
}

#######
# Main
#######

domain_home="$BEA_HOME/user_projects/domains/$WL_DOMAIN"
toolsbin_dir="$domain_home/tools/bin"
cd $domain_home

# Common functions. 
if [ -f tools/bin/installCommon.bash ]; then
    . tools/bin/installCommon.bash
else
    echo "tools/bin/installCommon.bash functions file not found."
    exit 1
fi

validateEnvironmentVars

checkInstalledServerType
startAllServers

