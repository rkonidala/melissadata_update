import os
import time as pytime
from weblogic.security.internal import SerializedSystemIni
from weblogic.security.internal.encryption import ClearOrEncryptedService

def defineGlobalVars():
    global weblogicPassword
    global adminHost
    global adminPort
    global action
    global domainDir
    global areManagedServersRunning

    weblogicPassword = sys.argv[1]
    adminHost = sys.argv[2]
    adminPort = sys.argv[3]
    action = sys.argv[4]
    domainDir = os.environ['BEA_HOME'] + '/user_projects/domains/' + os.environ['WL_DOMAIN']
    areManagedServersRunning =  True

def isServerRunning(serverName):
    status = getSLCRT(serverName)
    if status == "RUNNING":
        return True
    else:
        return False

def getSLCRT(svrName):
    cd('/ServerLifeCycleRuntimes/')
    cd(svrName)
    serverState=cmo.getState()
    return str(serverState)

def connectToAdminServer(adminHost, adminPort, weblogicPassword): 
   try:
      redirect("/dev/null")
      connect('weblogic', weblogicPassword, adminHost+':'+adminPort)
      stopRedirect()
   except:
      stopRedirect()
      print " "
      print "***"
      print "*** ERROR: 'admin' server is not running. Please start 'admin' server first, then try again"
      print "***"
      print " "
      sys.exit(1)

def isDonePulling(status):
    if status != "STARTING" and status != "STANDBY" and status != "RESUMING":
        return true
    else:
        return false;

def serverStatus(serverName, pullingStatus):
    global areManagedServersRunning

    status = getSLCRT(serverName)
    print 'Status of \"' + serverName + '\":\t' + status.lower().replace("_", " ")

    if status != 'RUNNING':
        areManagedServersRunning = False

    return isDonePulling(status)

def managedServersStatus(managedServers, pullingStatus):
   allServersDonePulling = true
   for ms in managedServers:
      serverName = ms.getName()
      if serverName != "admin" and serverName != "externaljms":
         isDonePulling = serverStatus(serverName, pullingStatus)
         if isDonePulling == false:
             allServersDonePulling = false
   return allServersDonePulling

def startManagedServer(serverName):
   if isServerRunning(serverName):
       print 'Server \"' + serverName + '\" has already started.\n'
       return true

   managedServerStatus = false
   redirect("/dev/null")
   try:
      if serverName == 'externaljms':
         # start JMS in block mode
         start(serverName, 'Server')
         managedServerStatus = true
      else:
         # start claim-server in non-block mode
         start(serverName, 'Server', block='false')
      stopRedirect()
      return managedServerStatus
   except:
      stopRedirect()
      print "*****Exception*****"
      print ' '
      if serverName == 'externaljms':
         printFailedStartJMSServer(serverName)
         sys.exit(1)
      else:
         print '***'
         print "*** ERROR: failed to start server '" + serverName + "'."
         print '***'
         print " "
         return false


def printFailedStartJMSServer(serverName):
    print '***'
    print "*** ERROR: failed to start server '" + serverName + "'. Please fix the issue, then try again."
    print '***'
    print " "

def startClaimServers(managedServers):
    allClaimServerStatus = true
    for ms in managedServers:
        csname = ms.getName()
        if csname != "admin" and csname != "externaljms":
            claimServerStatus = startManagedServer(csname)
            if claimServerStatus == false:
                allClaimServerStatus = false

    return allClaimServerStatus

def shutdownServer(serverName):
   if not isServerRunning(serverName):
       print 'Server \"' + serverName + '\" has already stopped.\n'
       return

   try:
      shutdown(serverName,'Server','true',0,'true','true')
   except:
      print " "
      print '***'
      print "*** Warning: failed to shut down server '" + serverName + "'."
      print '***'
      print " "

def shutdownClaimServers(managedServers):
   for ms in managedServers:
      serverName = ms.getName()
      if serverName != "admin" and serverName != "externaljms" :
         shutdownServer(serverName)

def waitInSeconds(seconds):
    for i in range(1, seconds):
        sys.stdout.write(".")
        sys.stdout.flush()
        pytime.sleep(1)
    print "\n"

def monitorServerStatus(servers):
    global areManagedServersRunning

    allServersDonePulling = false
    while allServersDonePulling == false:
        waitInSeconds(15)
        areManagedServersRunning = True
        allServersDonePulling = managedServersStatus(servers, true)

def exitOnError():
    if not areManagedServersRunning:
        print ''
        print '***'
        print '*** Some managed servers may not be running.'
        print '***'
        sys.exit(1)

#######
# Main
#######

defineGlobalVars()

encryptionService = SerializedSystemIni.getEncryptionService(domainDir)
coes = ClearOrEncryptedService(encryptionService)

connectToAdminServer(adminHost, adminPort, coes.decrypt(weblogicPassword))
servers=cmo.getServers()
domainRuntime()

if action == "status":
    serverStatus('externaljms', false)
    managedServersStatus(servers, false)
    exitOnError()
elif action == "start":
    if startManagedServer('externaljms') == true:
        if startClaimServers(servers) == false:
            # pull claim-servers status if not all servers started
            monitorServerStatus(servers)
    else:
        printFailedStartJMSServer('externaljms')
    exitOnError()
elif action == "stop":
    shutdownClaimServers(servers)
    shutdownServer('externaljms')
else:
    print 'Unknown action \"' + action + '\".'
