#!/usr/bin/env python
#Importing the modules
import os
import argparse
import re
import sys
import subprocess

#Define the arguments for getting inputs
parser = argparse.ArgumentParser(description='Update the Melissadata Components')
parser.add_argument('-a', '--address', type=str,  required=False, help='Address Component of Melissadata')
parser.add_argument('-g','--geocoder', type=str,  required=False, help='Geocoder Component of Melissadata')
parser.add_argument('-l','--license', type=str,  required=False, help='License Component of Melissadata')
parser.add_argument('--restart', action='store_true', default=False, required=False, help='Restart Apps')
parser.add_argument('-vvv', '--verbose', action='store_true', default=False, dest='verbose',required=False, help='Run ansible-playbook in verbose mode')
parser.add_argument('-t','--tags', type=str, default=False, dest='tags',required=False, help='Run ansible-playbook with tags')
parser.add_argument('--hosts', type=str, default=False, dest='hosts',required=False, help='Hosts file to run the playbook against')
args = parser.parse_args()

# Store the obtained inputs 
melissadata = {
               'address': args.address, 
			   'geocoder': args.geocoder, 
			   'license': args.license
			   }
# Parameter to run playbook in verbose mode
verbosity = args.verbose
# Filter the melissadata components needed for update
melissadata = dict((k,v) for k,v in melissadata.iteritems() if v )
# Parameter to check if Restart is required
perform_restart = args.restart
# Parameter to pass the tags
playbook_tags = args.tags

hosts = "%s/hosts" % os.path.abspath('.')

if args.hosts:
    try:
		os.path.isabs(args.hosts)
		hosts = os.path.abspath(args.hosts)
    except ValueError:
        print "%s needs to be absolute path" % args.hosts
else:
	hosts = "%s/hosts" % os.path.abspath('.')



# Method for validating input melissadata package format
def check_input_format(melissadata):
	for component,package in melissadata.iteritems():
		try:
			assert re.match(r"melissadata_%s_[0-9]+.tar.gz" % component, package)
			print "*** %s format: Validated ***" % component
		except AssertionError:
			print "Invalid input should be in format: melissadata_%s_<version>.tar.gz" % (component)
			sys.exit(1)

# Method to run the ansible_playbook
def run_playbook(playbook, hosts, vars=False,verbosity=False,tags=False):
    if type(vars) is dict:
        playbook_command = "ansible-playbook -i %s %s -e" % (hosts, playbook)
        playbook_command = playbook_command.split()
        playbook_command.append(str(vars))        
    else:
        playbook_command = "ansible-playbook -i %s %s" % (hosts, playbook)
        playbook_command = playbook_command.split()
    # Run the playbook with tags
    if playbook_tags:
    	playbook_command.append("--tags")
    	playbook_command.append(playbook_tags)
    # Run in verbose mode when defined
    if verbosity:
    	playbook_command.append("-vvv")
    print "Running Playbook:\n%s" % (' '.join(playbook_command))
    return subprocess.call(playbook_command)

#Execution Starts Here
if __name__ == '__main__':
	playbook = "%s/melissadata_update.yml" % os.path.abspath('.')
	check_input_format(melissadata)
	if melissadata:
		if run_playbook(playbook, hosts, melissadata, verbosity, playbook_tags) == 0:
			print "Melissadata Successful"
		else:
		    print "Melissadata Failed"

	if perform_restart:
		print "Restarting Apps"

